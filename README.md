# spring-config-transform

Spring-Boot Config Transform reads spring-boot configurations (application.properties / envionment / etc) and update a properties file with these values. It is used to normalize other applications designed an environment for spring-boot applications.

We use this tool with DSPACE 5x application to run it in a Docker envionment.