package br.mvpcortes.sbct;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;

import java.io.File;

/**
 * Store file with the dest properties
 */
@Scope("prototype")
@Getter
public class DestFileReference {

    private final File destFile;

    public DestFileReference(@Value("${sbct.dest.file}") File destFile) {
        this.destFile = destFile;
    }

    @Override
    public String toString() {
        return "DestFile: " + destFile.toString();
    }
}
