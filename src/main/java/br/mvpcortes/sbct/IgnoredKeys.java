package br.mvpcortes.sbct;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Get keys to ignore on spring-boot properties for transformation
 */
@Component
@Scope("prototype")
@Slf4j
public class IgnoredKeys {

    /**
     * Default patterns to ignore. We will ignore all properties started with sbct. because it can be used to config itself.
     */
    public final static List<String> LIST_DEFAULT_IGNORED_KEYS = List.of("sbct");

    private final Environment environment;

    private List<String> listIgnoredKeys;

    public IgnoredKeys(Environment environment) {
        this.environment = environment;
    }


    public boolean shouldNotBeIgnored(Map.Entry entry){
        return !shouldBeIgnored(entry);
    }


    /**
     * Verify if key of a Entry should be ignored.
     * @param entry A Map.Entry object
     * @return
     */
    public boolean shouldBeIgnored(Map.Entry entry){
        if(entry == null || entry.getKey() == null || entry.getKey().toString().trim().isEmpty())
            return true;

        return ignoredPropertiesStream()
                .anyMatch(ignoredKeys->entry.getKey().toString().startsWith(ignoredKeys));
    }


    /**
     * Get string patterns to ignore using a stream.
     * @return
     */
    private Stream<String> ignoredPropertiesStream(){
        if(listIgnoredKeys == null){
            listIgnoredKeys = Stream.concat(getKeys(), LIST_DEFAULT_IGNORED_KEYS.stream())
                    .filter(Objects::nonNull)
                    .map(String::trim)
                    .map(IgnoredKeys::removeQuotes)
                    .map(IgnoredKeys::removeLastDot)
                    .distinct()
                    .collect(Collectors.toList());

            log.info("Ignored keys: {}", listIgnoredKeys);
        }

        return listIgnoredKeys.stream();
    }

    public static String removeLastDot(String string) {

        if(string == null || string.isEmpty()){
            return string;
        }

        string = string.trim();

        while(string.endsWith(".")){
            string = string.substring(0, string.length()-1);
        }
        return string;
    }

    private Stream<String> getKeys() {
        return Stream.of(Optional.ofNullable(environment.getProperty("sbct.ignore.keys")).orElse("")
                .split(";"))
                .filter(Objects::nonNull)
                .map(String::trim)
                .filter(str->!str.isEmpty());
    }

    public static String removeQuotes(String str){

        if(str == null || str.isEmpty()) {
            return str;
        }

        str = str.trim();

        while(!str.isEmpty() && '\'' == str.charAt(0)){
            str = str.substring(1);
        }

        while(!str.isEmpty() && '\'' == str.charAt(str.length()-1)){
            str = str.substring(0,str.length()-1);
        }

        return str;
    }
}
