package br.mvpcortes.sbct;

import lombok.val;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbctApplication {

	public static void main(String[] args) {
		try(val configurableApplicationContext = SpringApplication.run(SbctApplication.class, args)) {
			configurableApplicationContext.getBean(SbctRunner.class).run();
		}
	}

}
