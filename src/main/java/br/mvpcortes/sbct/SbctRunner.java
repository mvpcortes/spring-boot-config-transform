package br.mvpcortes.sbct;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.*;
import java.util.Properties;

@Service
@Slf4j
public class SbctRunner implements Runnable {

    public final static String STR_BKP_SUFFIX = ".bkp";

    private final IgnoredKeys ignoredKeys;

    private final Environment environment;

    private final DestFileReference destFileReference;

    public SbctRunner(DestFileReference destFileReference, Environment environment, IgnoredKeys ignoredKeys) {
        this.ignoredKeys = ignoredKeys;
        this.environment = environment;
        this.destFileReference = destFileReference;
    }

    @Override
    public void run() {
        log.info("Init SBCT");

        try {
            dealBackup();
        }catch(Exception e){
            throw new IllegalStateException("Cannot deal with backup. " + e.getMessage(), e);
        }

        Properties properties;
        try {
            properties = loadDestProperties();
        }catch(Exception e){
            throw new IllegalStateException("Cannot load properties. " + e.getMessage(), e);
        }

        properties = transformProperties(properties);


        try {
            saveProperties(properties);
        }catch (Exception e){
            throw new IllegalStateException("Cannot save properties. " + e.getMessage(), e);
        }

        log.info("Finish SBCT");
    }

    /**
     * Transformation work in this way:
     * * Verify if should be ignored using {IgnoredKeys};
     * * Verify if this properties exists on spring-boot environment (application.properties, etc);
     * * If it is defined then update value on destiny file.
     * @param properties
     * @return
     */
    protected Properties transformProperties(Properties properties) {
        log.info("Transforming properties");
        properties.entrySet()
                .stream()
                .filter(ignoredKeys::shouldNotBeIgnored)
                .forEach(entry -> {
                    final String key = entry.getKey().toString().trim();
                    final String newValue = environment.getProperty(key);
                    if (newValue != null) {
                        log.debug("Update value of key {}: {} to {}", entry.getKey(), entry.getValue(), newValue);
                        entry.setValue(newValue);
                    }
                });

        return properties;
    }


    /**
     * Save transformedProperties on Dest File.
     * @param transformedProperties the transformed properties to save
     */
    protected void saveProperties(Properties transformedProperties) throws IOException {
        try(val writer = new FileWriter(getDestFile())){
            log.info("Saving properties in {}", getDestFile());
            transformedProperties.store(writer, "Properties transformed by SBCT");
        }
    }

    protected Properties loadDestProperties() throws IOException {
        log.info("Loading properties from desc file (type: properties)");
        Properties properties = new Properties();
        try (Reader reader = new FileReader(getDestFile())) {
            properties.load(reader);
        }
        return properties;
    }

    protected void dealBackup() throws IOException {
        log.info("Deal Backup");
        val fileBkp = getFileBackup();

        if (fileBkp.exists()) {
            log.info("Restoring Backup");
            FileCopyUtils.copy(fileBkp, getDestFile());
        } else {
            fileBkp.createNewFile();
            log.info("Creating Backup");
            FileCopyUtils.copy(getDestFile(), fileBkp);
        }
    }

    private File getFileBackup(){
        return new File(getDestFile().getParentFile(), getDestFile().getName() + STR_BKP_SUFFIX);
    }

    protected File getDestFile() {

        val destFile = destFileReference.getDestFile();

        if(!destFile.exists()){
            throw new IllegalStateException("The destiny File '" + destFile + "' not exists.");
        }

        if(!destFile.isFile()){
            throw new IllegalStateException("The destiny File '" + destFile + "' is not a file.");
        }

        return destFile;
    }
}
