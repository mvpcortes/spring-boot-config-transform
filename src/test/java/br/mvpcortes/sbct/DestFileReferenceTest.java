package br.mvpcortes.sbct;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

class DestFileReferenceTest {

    @TempDir File tempDir;

    File fileDest;

    DestFileReference destFileReference;

    @BeforeEach
    public void init(){
        try {
            fileDest = new File(tempDir, "temp");
            fileDest.createNewFile();
            destFileReference = new DestFileReference(fileDest);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Test
    public void verify_if_get_file_reference_will_work(){
        assertThat(destFileReference.getDestFile()).isEqualTo(fileDest);
    }

    @Test
    public void verify_toString() {
        assertThat(destFileReference.toString()).isEqualTo("DestFile: " + fileDest.toString());
    }

}