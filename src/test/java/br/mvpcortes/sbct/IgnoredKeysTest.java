package br.mvpcortes.sbct;


import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.core.env.Environment;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IgnoredKeysTest {

    @ParameterizedTest
    @ValueSource(strings = {
            " | ",
            "a|a",
            "'| ",
            "''| ",
            "'a'|a",
            "'a|a",
            "a'|a",
            "'xuxu'|xuxu",
            "'xu'a'xu'|xu'a'xu",
            "''xu'a'xu''|xu'a'xu"
    })
    public void verify_if_will_remove_quotes(String args) {
        val source = args.split("\\|")[0].trim();
        val result = args.split("\\|")[1].trim();
        assertThat(IgnoredKeys.removeQuotes(source)).isEqualTo(result);
    }

    @Test
    public void verify_if_remove_quote_will_deal_with_null_argument(){
        assertThat(IgnoredKeys.removeQuotes(null)).isNull();
    }

    @Test
    public void verify_if_remove_quote_will_deal_with_empty_argument(){
        assertThat(IgnoredKeys.removeQuotes("")).isEmpty();
    }


    @Test
    public void verify_if_remove_last_dot_will_work_with_null(){
        assertThat(IgnoredKeys.removeLastDot(null)).isNull();
    }

    @Test
    public void verify_if_remove_last_dot_will_work_with_empty(){
        assertThat(IgnoredKeys.removeLastDot(" ")).isEmpty();
    }

    @ParameterizedTest
    @ValueSource(strings = {
            " | ",
            "a|a",
            "a.|a",
            "a.big.greater.property|a.big.greater.property",
            "a.big.greater.property.|a.big.greater.property",
            "a.big.greater.property.with.a.lot.of.dots.....|a.big.greater.property.with.a.lot.of.dots",
    })
    public void verify_if_remove_last_dot_will_work(String args){
        val source = args.split("\\|")[0].trim();
        val result = args.split("\\|")[1].trim();
        assertThat(IgnoredKeys.removeLastDot(source)).isEqualTo(result);
    }

    @Test
    public void verify_if_should_be_ignored_will_ignore_null_entries(){
        val ignoredKeys = new IgnoredKeys(mock(Environment.class));
        assertThat(
                ignoredKeys.shouldBeIgnored(null)
        ).isTrue();
    }

    @Test
    public void verify_if_should_be_ignored_will_ignore_null_key_entries(){
        val ignoredKeys = new IgnoredKeys(mock(Environment.class));
        val entry = mock(Map.Entry.class);
        when(entry.getKey()).thenReturn(null);
        assertThat(
                ignoredKeys.shouldBeIgnored(entry)
        ).isTrue();
    }

    @Test
    public void verify_if_should_be_ignored_will_ignore_empty_key_entries(){
        val ignoredKeys = new IgnoredKeys(mock(Environment.class));
        assertThat(
                ignoredKeys.shouldBeIgnored(Map.entry("", "x"))
        ).isTrue();
    }

    @Test
    public void verify_if_should_be_ignored_will_ignore_blank_key_entries(){
        val ignoredKeys = new IgnoredKeys(mock(Environment.class));
        assertThat(
                ignoredKeys.shouldBeIgnored(Map.entry("    ", "x"))
        ).isTrue();
    }


    @ParameterizedTest
    @ValueSource(strings = {
            "prop.one",
            "prop.two",
            "prop.three",
    })
    public void verify_if_should_not_be_ignored_values(String strProperty){
        Environment environment = mock(Environment.class);

        when(environment.getProperty("sbct.ignore.keys")).thenReturn("prop.one;prop.two;prop.three;   ;   ; ");


        val ignoredKeys = new IgnoredKeys(environment);

        assertThat(ignoredKeys.shouldNotBeIgnored(Map.entry(strProperty, ""))).isFalse();
    }
}
