package br.mvpcortes.sbct;

import lombok.Getter;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Properties;

import static org.mockito.Mockito.mock;

class SbctRunnerExceptionTest extends SbctRunner {

    public static class SbctRunnerMocked extends SbctRunner{

        @SuppressWarnings("CanBeFinal")
        @Getter
        protected Properties properties = mock(Properties.class);

        public SbctRunnerMocked() {
            super(null, null, null);
        }

        @Override
        protected void dealBackup() throws IOException {}

        @Override
        protected Properties transformProperties(Properties properties) {return properties;}

        @Override
        protected void saveProperties(Properties transformedProperties) throws IOException {}

        @Override
        protected Properties loadDestProperties() throws IOException {return properties;}
    }

    public SbctRunnerExceptionTest() {
        super(null, null, null);
    }

    @Test
    public void when_not_have_exceptions_then_run_ok(){
        SbctRunnerMocked sbctRunnerMocked = new SbctRunnerMocked();

        sbctRunnerMocked.run();
    }

    @Test
    public void when_has_exception_on_deal_backup_then_propagate_exception(){
        SbctRunnerMocked sbctRunnerMocked = new SbctRunnerMocked(){
            @Override
            protected void dealBackup() throws IOException {
                throw new IOException("dealBackupException");
            }
        };

        Assertions.assertThatExceptionOfType(IllegalStateException.class)
                .isThrownBy(sbctRunnerMocked::run)
                .withMessage("Cannot deal with backup. dealBackupException")
                .withCauseExactlyInstanceOf(IOException.class);
    }

    @Test
    public void when_has_exception_on_load_properties_then_propagate_exception(){
        SbctRunnerMocked sbctRunnerMocked = new SbctRunnerMocked(){
            @Override
            protected Properties loadDestProperties() throws IOException {
                throw new IOException("loadDestProperties");
            }
        };

        Assertions.assertThatExceptionOfType(IllegalStateException.class)
                .isThrownBy(sbctRunnerMocked::run)
                .withMessage("Cannot load properties. loadDestProperties")
                .withCauseExactlyInstanceOf(IOException.class);
    }

    @Test
    public void when_has_exception_on_save_properties_then_propagate_exception(){
        SbctRunnerMocked sbctRunnerMocked = new SbctRunnerMocked(){
            @Override
            protected void saveProperties(Properties properties) throws IOException {
                throw new IOException("saveProperties");
            }
        };

        Assertions.assertThatExceptionOfType(IllegalStateException.class)
                .isThrownBy(sbctRunnerMocked::run)
                .withMessage("Cannot save properties. saveProperties")
                .withCauseExactlyInstanceOf(IOException.class);
    }
}