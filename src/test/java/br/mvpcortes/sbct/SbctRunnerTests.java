package br.mvpcortes.sbct;

import lombok.val;
import org.assertj.core.api.AbstractStringAssert;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.contentOf;

@SpringBootTest
class SbctRunnerTests {

	@TempDir
	File tempDir;

	File destFile;


	@Autowired
	private ConfigurableEnvironment environment;

	@MockBean
	private DestFileReference destFileReference;

	@Autowired
	SbctRunner sbctRunner;

	@BeforeEach
	public void init() throws IOException {
		destFile = new File(tempDir, "my_application_properties.properties");

		destFile.createNewFile();

		//mocking file reference;
		Mockito.doReturn(destFile).when(destFileReference).getDestFile();

	}

	/**
	 * We do not need delete file {destFileProperties} because JUnit 5 will delete the {tempDir}
	 */
	@AfterEach
	public void destroy(){
		//do nothing
	}

	@Test
	public void when_dest_file_is_a_directory_then_fail(){
		Assertions.assertThatExceptionOfType(IllegalStateException.class)
				.isThrownBy(()->{
					val file = new File(tempDir, "directory");
					file.mkdir();
					Mockito.doReturn(file).when(destFileReference).getDestFile();

					sbctRunner.run();
				})
				.withMessageMatching("Cannot deal with backup. The destiny File \'.+\' is not a file\\.")
				.withRootCauseInstanceOf(IllegalStateException.class);
	}


	@Test
	public void when_dest_file_not_exists_then_fail(){
		Assertions.assertThatExceptionOfType(IllegalStateException.class)
				.isThrownBy(()->{
					Mockito.doReturn(new File(tempDir, "not_existed_file")).when(destFileReference).getDestFile();

					sbctRunner.run();
				})
				.withMessageMatching("Cannot deal with backup. The destiny File \'.+\' not exists\\.")
				.withRootCauseInstanceOf(IllegalStateException.class);
	}


	@Test
	public void when_there_is_not_properties_in_dest_file_then_not_change_it() {
		sbctRunner.run();

		//verify if file as only two new lines. The header and the date.
		assertHeader()
				.hasLineCount(2)
				;
	}

	@Test
	public void when_there_is_one_property_in_file_not_exists_in_spring_environment_then_not_change_it() {

		this.fillDestFile("my.property.not_changed", "some_value");

		sbctRunner.run();

		//verify if file as only two new lines. The header and the date.
		assertHeader()
				.contains("my.property.not_changed=some_value");
	}


	@Test
	public void when_there_is_one_property_to_change_then_change_it() {
		initOneProperty();
		sbctRunner.run();

		//verify if file as only two new lines. The header and the date.
		assertHeader()
				.hasLineCount(3)
				.doesNotContain("a.very.important.property=old_value")
				.contains("a.very.important.property=important_value");
	}


	@Test
	public void when_there_is_one_property_then_create_backup_with_old_value() {
		initOneProperty();

		val STR_FILE_BKP_NAME = destFile.getName() + SbctRunner.STR_BKP_SUFFIX;
		val fileBackup = new File(destFile.getParent(), STR_FILE_BKP_NAME);
		assertThat(fileBackup).doesNotExist();

		sbctRunner.run();

		assertThat(fileBackup).exists();

		assertThat(contentOf(fileBackup))
				.hasLineCount(1)
				.contains("a.very.important.property=old_value");

		assertThat(contentOf(destFile))
				.hasLineCount(3)
				.contains("a.very.important.property=important_value");
	}


	@Test
	public void when_there_is_one_property_and_run_twice_then_restore_backup() throws Exception {
		initOneProperty();
		when_there_is_one_property_then_create_backup_with_old_value();

		//this new value will not persist because the backup will be restored
		fillDestFile(
				"xuxu.xaxa", "xuxu.xaxa");

		sbctRunner.run();
		val STR_FILE_BKP_NAME = destFile.getName() + SbctRunner.STR_BKP_SUFFIX;
		val fileBackup = new File(destFile.getParent(), STR_FILE_BKP_NAME);

		assertThat(contentOf(fileBackup))
				.hasLineCount(1)
				.contains("a.very.important.property=old_value");

		assertThat(contentOf(destFile))
				.hasLineCount(3)
				.contains("a.very.important.property=important_value");
	}


	public void initOneProperty(){
		fillDestFile("a.very.important.property", "old_value");
		addPropeties("a.very.important.property", "important_value");
	}


	@Test
	public void when_there_is_two_property_in_file_equal_to_three_in_spring_enviornment_then_change_only_two() {

		this.addPropeties(
				"first.very.important.property", "I'm first baby!",
				"second.very.important.property", "'First is first. The second is nobody'. Maia, Tim. (Brazilian version of 'Looser' expression.");
		this.fillDestFile(
				"first.very.important.property", "lala",
				"second.very.important.property", "lele",
				"third.non.important.property", "Oh no, I will not be copied to properties file!");

		sbctRunner.run();

		//verify if file as only two new lines. The header and the date.
		assertHeader()
				.hasLineCount(5)
				.doesNotContain("lala")
				.doesNotContain("lele")
				.contains("first.very.important.property=I'm first baby\\!")
				.contains("second.very.important.property='First is first. The second is nobody'. Maia, Tim. (Brazilian version of 'Looser' expression.")
				.contains("third.non.important.property=Oh no, I will not be copied to properties file\\!");
	}

	@Test
	public void when_there_is_implicit_ignored_keys_then_ignore_it() {

		this.fillDestFile("a.very.important.property", "old_value",
		"sbct.property",  "value will not change");
		this.addPropeties("a.very.important.property", "important_value",
				"sbct.property","I shall be ignored");

		sbctRunner.run();

		//verify if file as only two new lines. The header and the date.
		assertHeader()
				.hasLineCount(4)
				.contains("a.very.important.property=important_value")
				.contains("sbct.property=value will not change")
				.doesNotContain("I should be ignored");
	}

	@Test
	public void when_there_is_explicit_ignored_keys_then_ignore_it() {

		this.fillDestFile(
				"a.very.important.property", "old_value",
				"an.ignored.key", "value will not be changed"
		);

		this.addPropeties(
				"a.very.important.property", "important_value",
				"an.ignored.key", "I should be ignored :(",
				"sbct.ignore.keys","an.ignored");

		sbctRunner.run();

		//verify if file as only two new lines. The header and the date.
		assertHeader()
				.hasLineCount(4)
				.contains("a.very.important.property=important_value")
				.contains("an.ignored.key=value will not be changed")
				.doesNotContain("I should be ignored :(");
	}

	@Test
	public void when_there_are_two_explicit_ignored_keys_then_ignore_all_them() {

		this.fillDestFile("a.very.important.property", "old_value",
				"an.ignored.key", "value will not be changed",
				"an.ignored.key.2", "I will not changed",
				"another.ignored.key", "xaxa will not show");

		this.addPropeties("a.very.important.property", "important_value",
				"an.ignored.key", "xuxu will not show",
				"another.ignored.key", "I change",
				"an.ignored.key.2", "Changed",
				"sbct.ignore.keys","an.ignored.key; another.ignored.key");

		sbctRunner.run();

		//verify if file as only two new lines. The header and the date.
		assertHeader()
				.hasLineCount(6)
				.contains("a.very.important.property=important_value")
				.contains("an.ignored.key=value will not be changed")
				.contains("an.ignored.key.2=I will not changed")
				.contains("another.ignored.key=xaxa will not show")
				.doesNotContain("xuxu will not show")
				.doesNotContain("I change")
				.doesNotContain("Changed");
	}

	/**
	 * Add properties in spring
	 * @param values
	 */
	public void addPropeties(String... values){
		val mapPropertySource = getTestPropertySource();
		iterateInTwo(values, mapPropertySource::put);
	}

	@Test
	public void when_try_restore_a_backup_file_then_ok() {

		this.fillDestFile("a.very.important.property", "old_value");
		this.addPropeties("a.very.important.property", "important_value");

		sbctRunner.run();

		//verify if file as only two new lines. The header and the date.
		assertHeader()
				.hasLineCount(3)
				.doesNotContain("old_value")
				.contains("a.very.important.property=important_value");
	}


	private Map<String, Object> getTestPropertySource() {
		val sources = environment.getPropertySources();
		if (sources.contains("test")) {
			return (Map<String, Object>)sources.get("test").getSource();
		} else {
			final Map<String, Object> map = new HashMap();
			sources.addFirst(new MapPropertySource("test", map));
			return map;
		}
	}

	/**
	 * fill dest file with properties for test
	 * @param keyValue key-value pair in a array. Like Map.of
	 */
	private void fillDestFile(String... keyValue){
		try(PrintWriter writer = new PrintWriter(this.destFile)) {
			iterateInTwo(keyValue, (key, value)-> writer.printf("%s=%s%n", key, value));
		} catch (FileNotFoundException e) {
			throw new IllegalStateException("Cannot write values in DestFile", e);
		}
	}

	private <T> void iterateInTwo(T[] values, BiConsumer<T, T> consumer){
		for (var i = 0; i < values.length - 1; i += 2) {
			consumer.accept(values[i], values[i + 1]);
		}
	}

	private AbstractStringAssert<?> assertHeader() {
		return assertThat(contentOf(this.destFile))
				.startsWith("#Properties transformed by SBCT")
				.containsPattern("#\\w{3}\\s\\w{3}\\s\\d{2}\\s\\d{2}:\\d{2}:\\d{2}\\s\\w{3}\\s\\d{4}");
	}
}
